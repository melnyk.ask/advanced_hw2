let burger = document.querySelector(".burger");
let burger__one = document.querySelector(".burger__one");
let burger__two = document.querySelector(".burger__two");
let burger__three = document.querySelector(".burger__three");
let burger__nav = document.querySelector(".burger__nav");
let nav2__item = document.querySelectorAll(".nav2__item");
let tabs = [];
let burger_tabs = [];

for (let i = 0; i <= 6; i++) { 
    tabs[i] = document.querySelector(`.tab${i}`);
    burger_tabs[i] = document.querySelector(`.burger_tab${i}`);  
}

tabs[0].classList.add("active");
burger_tabs[0].classList.add("active");

for (let i = 0; i <= 6; i++) {
    tabs[i].onclick = () => { 
        for (let j = 0; j <= 6; j++) { 
            if (j !== i) {
                tabs[j].classList.remove("active");
                burger_tabs[j].classList.remove("active");
            }
            else { 
                tabs[i].classList.add("active");
                burger_tabs[i].classList.add("active");
            } 
        }
        
    }
    burger_tabs[i].onclick = () => { 
        for (let j = 0; j <= 6; j++) { 
            if (j !== i) {
                tabs[j].classList.remove("active");
                burger_tabs[j].classList.remove("active");
            }
            else { 
                tabs[i].classList.add("active");
                burger_tabs[i].classList.add("active");
            } 
        }
    }
}

// for (let i = 0; i <= 6; i++) { 
//     if () { }
//     tabs[i].classList.remove("active");
// }

nav2__item.forEach(
    (elem) => { 
        elem.onclick = () => { 
            burger__nav.classList.add("hidden");
            burger__one.classList.remove("burger_one_press");
            burger__two.classList.remove("hidden");
            burger__three.classList.remove("burger_three_press");
        }
    }
);

let flag = 0;
burger.onclick = () => { 
    // if (flag === 0) { 
    //     burger__nav.classList.remove("hidden");
    //     burger__one.classList.add("burger_one_press");
    //     burger__two.classList.add("hidden");
    //     burger__three.classList.add("burger_three_press");
    // }
        
    // if (flag === 1) {     
    //     burger__nav.classList.add("hidden");  
    //     flag = 0;
    // }
    // flag = 1;
    
        burger__nav.classList.toggle("hidden");
        burger__one.classList.toggle("burger_one_press");
        burger__two.classList.toggle("hidden");
        burger__three.classList.toggle("burger_three_press");    
}

